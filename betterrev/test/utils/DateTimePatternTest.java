package utils;

import org.joda.time.DateTime;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

/**
 * Basic test for DateTimeUtil
 *
 * @see utils.DateTimePattern
 * @see utils.Pattern
 *
 */
public class DateTimePatternTest {

    public static final long CHRISTMAS_1999_161420 = 946138460645L;

    @Test
    public void format_WithValidInputs() {
        assertThat((DateTimePattern.format(new DateTime())), is(not(nullValue())));
    }

    @Test
    public void format_WithValidInputDateTimeAndDefaultPatterns() {
        assertThat((DateTimePattern.DEFAULT.formatDateTime(new DateTime())), is(not(nullValue())));
    }

    @Test
    public void format_OldDateTimeReturnsDefaultStringRepresentation() {
        DateTime oldDateTime = new DateTime(CHRISTMAS_1999_161420);
        assertThat(DateTimePattern.format(oldDateTime), is("25 December 1999 16:14:20"));
    }

    @Test(expected = NullPointerException.class)
    public void format_WithNullDateTimeAndDefaultPatterns_ThrowsNPE() {
        DateTimePattern.DEFAULT.formatDateTime(null);
    }

}
