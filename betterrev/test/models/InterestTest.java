package models;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;

/**
 * Basic Interest entity persistence tests.
 */
public class InterestTest extends AbstractPersistenceIntegrationTest {

    private static final String TEST_INTEREST_PATH = "a/b/c";
    private static final String TEST_INTEREST_PROJECT = "a@b.com";

    public static Interest createTestInstance() {
        return new Interest(TEST_INTEREST_PATH, TEST_INTEREST_PROJECT);
    }

    @Test
    public void creation_beforeSave_shouldHaveNullId() {
        Interest interest = createTestInstance();
        assertThat(interest.id, is(nullValue()));
    }

    @Test
    public void save_validInterest_entityPersisted() {
        Interest interest = createTestInstance();
        interest.save();
        assertThat(interest.id, is(not(nullValue())));
    }
}
