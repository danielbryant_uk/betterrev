package models;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.EbeanServer;
import com.avaje.ebean.config.ServerConfig;
import com.avaje.ebean.config.dbplatform.H2Platform;
import com.avaje.ebeaninternal.api.SpiEbeanServer;
import com.avaje.ebeaninternal.server.ddl.DdlGenerator;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import play.test.FakeApplication;
import play.test.Helpers;

/**
 * Base class to support integration-style testing of persistence layer.
 */
public abstract class AbstractPersistenceIntegrationTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractPersistenceIntegrationTest.class);

    private static final String EBEAN_SERVER_NAME = "default";

    protected static FakeApplication app;
    protected static DdlGenerator ddl;
    protected static EbeanServer server;

    @BeforeClass
    public static void setup() {
        app = Helpers.fakeApplication(Helpers.inMemoryDatabase());
        Helpers.start(app);

        server = Ebean.getServer(EBEAN_SERVER_NAME);

        LOGGER.info("Started Fake Application...");

        ServerConfig config = new ServerConfig();

        ddl = new DdlGenerator();
        ddl.setup((SpiEbeanServer) server, new H2Platform(), config);

        LOGGER.info("Initialised DDL Generator...");
    }

    @AfterClass
    public static void stopApp() {
        Helpers.stop(app);
        LOGGER.info("...stopped Fake Application");
    }

}
