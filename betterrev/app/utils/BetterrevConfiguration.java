package utils;


import play.Configuration;
import play.Play;

public enum BetterrevConfiguration {
    
    INST;

    private Configuration configuration = Play.application().configuration();

    public String owner() {
        return configuration.getString("owner");
    }

    public String project() {
        return configuration.getString("project");
    }
    
}