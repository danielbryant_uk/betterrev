package models;

import org.joda.time.DateTime;
import play.db.ebean.Model;
import play.utils.dao.BasicModel;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * ContributionEvent that represents a lifecycle event associated with a specific
 * Contribution.
 */
@Entity
public class ContributionEvent extends Model implements BasicModel<Long> {

    private static final long serialVersionUID = 188525469548289315L;

    public static Model.Finder<Long, ContributionEvent> find = new Model.Finder<>(Long.class, ContributionEvent.class);

    @GeneratedValue(strategy = IDENTITY)
    @Id
    public Long id;

    @ManyToOne
    public Contribution contribution;

    @Enumerated(EnumType.STRING)
    public ContributionEventType contributionEventType;

    public String linkToExternalInfo;

    public DateTime createdDate;

    private ContributionEvent() {
        this.createdDate = new DateTime();
    }

    public ContributionEvent(ContributionEventType contributionEventType) {
        this();
        this.contributionEventType = contributionEventType;
    }

    public ContributionEvent(ContributionEventType contributionEventType, Contribution contribution) {
        this(contributionEventType);
        this.contribution = contribution;
    }

    public ContributionEvent(ContributionEventType contributionEventType, String linkToExternalInfo) {
        this(contributionEventType);
        this.linkToExternalInfo = linkToExternalInfo;
    }

    @Override
    public String toString() {
        return "ContributionEvent [id=" + id + ", contribution=" + contribution + ", contributionEventType="
                + contributionEventType + ", linkToExternalInfo=" + linkToExternalInfo + ", createdDate=" + createdDate
                + "]";
    }

    @Override
    public Long getKey() {
        return id;
    }

    @Override
    public void setKey(Long key) {
        id = key;
    }
}
