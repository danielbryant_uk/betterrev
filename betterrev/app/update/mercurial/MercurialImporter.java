package update.mercurial;

import com.aragost.javahg.BaseRepository;
import com.aragost.javahg.Changeset;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import play.Logger;
import utils.Processes;

import com.aragost.javahg.Repository;
import com.aragost.javahg.commands.flags.LogCommandFlags;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Pulls in code from a Mercurial repository, keeping an internal hg repository up to date.
 */
public class MercurialImporter {

    private static final DateTimeFormatter FORMAT = DateTimeFormat.forPattern("> EEE MMM dd HH:mm:ss YYYY Z");

    private static final String IMPORT_SCRIPT = "get_source.sh";

    private static final List<String> SUB_REPOS = Arrays.asList("corba", "hotspot", "jaxp", "jaxws", "jdk", "langtools", "nashorn");

    // TODO See Issue #14 This value is never used
    private final String adoptDirectory;
    
    private final String topLevelRepository;

    public MercurialImporter(String adoptDirectory, String topLevelRepoName) {
        this.adoptDirectory = adoptDirectory;
        topLevelRepository = adoptDirectory + "/" + topLevelRepoName;
    }

    public int doImport() {
        try {
            Logger.debug("About to run [" + topLevelRepository + "/" + IMPORT_SCRIPT + "]");
            return Processes.runThroughShell(topLevelRepository, IMPORT_SCRIPT);
        } catch (IOException e) {
            Logger.error(e.getMessage(), e);
            return Integer.MIN_VALUE;
        }
    }

    public Map<String, List<Changeset>> listChangesets(DateTime lastImport) {
        Map<String, List<Changeset>> changes = new HashMap<>();

        for (String repositoryName : SUB_REPOS) {
            File repositoryLocation = new File(topLevelRepository, repositoryName);
            BaseRepository repository = Repository.open(repositoryLocation);
            try {
                List<Changeset> changesets = LogCommandFlags.on(repository)
                        .date(formatDate(lastImport))
                        .execute();

                if (!changesets.isEmpty()) {
                    changes.put(repositoryName, changesets);
                }
            } finally {
                repository.close();
            }
        }

        return changes;
    }

    public static String formatDate(DateTime dateTime) {
        return FORMAT.print(dateTime);
    }

}
