package update.ocachecker;

import models.ContributionEvent;
import models.User;
import models.User.OcaStatus;
import play.Logger;
import utils.OracleContributorAgreementService;
import utils.exception.OCAComunicationException;

public class CheckOcaStatus {

    public static boolean check(ContributionEvent request) {
        User user = request.contribution.requester;
        boolean hasSigned = false;
        if (user.ocaStatus == OcaStatus.UNKNOWN) {
            try {
                hasSigned = OracleContributorAgreementService
                        .hasSignedOCAforOpenJDK(user.name);
            } catch (OCAComunicationException e) {
                Logger.error(e.getMessage(), e);
            }
        } else if (user.ocaStatus == OcaStatus.SIGNED) {
            hasSigned = true;
        } else if (user.ocaStatus == OcaStatus.NOT_SIGNED) {
            hasSigned = false;
        }
        return hasSigned;
    }
}
